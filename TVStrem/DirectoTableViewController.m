//
//  DirectoTableViewController.m
//  TVStrem
//
//  Created by Jonathan Martínez López on 26/11/14.
//  Copyright (c) 2014 Máster Móviles. All rights reserved.
//

#import "DirectoTableViewController.h"
#import "UAMedio.h"
#import <MediaPlayer/MediaPlayer.h>

@interface DirectoTableViewController ()

@end

@implementation DirectoTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UAMedio *medio1 = [[UAMedio alloc] init];
    medio1.titulo = @"Canal 1";
    medio1.url = [NSURL URLWithString:@"http://10.0.1.143:1935/live/canal1/playlist.m3u8"];
    
    UAMedio *medio2 = [[UAMedio alloc] init];
    medio2.titulo = @"Canal 2";
    medio2.url = [NSURL URLWithString:@"http://10.0.1.143:1935/live/canal2/playlist.m3u8"];

    UAMedio *medio3 = [[UAMedio alloc] init];
    medio3.titulo = @"Canal 3";
    medio3.url = [NSURL URLWithString:@"http://10.0.1.143:1935/live/canal3/playlist.m3u8"];
    self.videos = [NSArray arrayWithObjects:medio1, medio2, medio3, nil];
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return [self.videos count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CellDirecto" forIndexPath:indexPath];
    
    // Configure the cell...
    UAMedio *medio = [self.videos objectAtIndex:indexPath.row];
    cell.textLabel.text = medio.titulo;
    
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    UAMedio *medio = [self.videos objectAtIndex:indexPath.row];
    MPMoviePlayerViewController *controller = [[MPMoviePlayerViewController alloc] initWithContentURL:medio.url];
    [self presentMoviePlayerViewControllerAnimated: controller];
    [controller.moviePlayer play];
    
}


/*
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:<#@"reuseIdentifier"#> forIndexPath:indexPath];
    
    // Configure the cell...
    
    return cell;
}
*/

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
