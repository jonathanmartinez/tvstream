//
//  RadioTableViewController.m
//  TVStrem
//
//  Created by Jonathan Martínez López on 26/11/14.
//  Copyright (c) 2014 Máster Móviles. All rights reserved.
//

#import "RadioTableViewController.h"
#import "UAMedio.h"

@interface RadioTableViewController ()

@end

@implementation RadioTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UAMedio *medio1 = [[UAMedio alloc] init];
    medio1.titulo = @"Steph Mike - Survive";
    //hay que bajarse la musica y sustituirla por zelda y starlight
    medio1.url = [[NSBundle mainBundle] URLForResource:@"Cancion1" withExtension:@"mp3"];
    
    UAMedio *medio2 = [[UAMedio alloc] init];
    medio2.titulo = @"Josh Wiz - Arround me";
    medio2.url = [[NSBundle mainBundle] URLForResource:@"Cancion2" withExtension:@"mp3"];
    
    self.audios = [NSArray arrayWithObjects:medio1, medio2, nil];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return [self.audios count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    
    // Configure the cell...
    UAMedio *medio = [self.audios objectAtIndex:indexPath.row];
    cell.textLabel.text = medio.titulo;
    
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    UAMedio *medio = [self.audios objectAtIndex:indexPath.row];
    self.player = [[AVAudioPlayer alloc] initWithContentsOfURL:medio.url error:nil];
    [self.player play];
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
