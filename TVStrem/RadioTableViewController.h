//
//  RadioTableViewController.h
//  TVStrem
//
//  Created by Jonathan Martínez López on 26/11/14.
//  Copyright (c) 2014 Máster Móviles. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>

@interface RadioTableViewController : UITableViewController

@property (nonatomic,strong) NSArray *audios;
@property (nonatomic,strong) AVAudioPlayer *player;

@end
