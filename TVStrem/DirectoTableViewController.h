//
//  DirectoTableViewController.h
//  TVStrem
//
//  Created by Jonathan Martínez López on 26/11/14.
//  Copyright (c) 2014 Máster Móviles. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DirectoTableViewController : UITableViewController

@property (nonatomic,strong) NSArray *videos;
@property (nonatomic,strong) NSURL *movieUrl;

@end
