//
//  UAMedio.h
//  TVStrem
//
//  Created by Máster Móviles on 26/11/14.
//  Copyright (c) 2014 Máster Móviles. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface UAMedio : NSObject

@property (nonatomic,strong) NSString *titulo;
@property (nonatomic,strong) NSString *artista;
@property (nonatomic,strong) NSString *album;
@property (nonatomic,strong) UIImage *caratula;
@property (nonatomic,strong) NSString *httpURL;
@property (nonatomic,strong) NSURL *url;

@end
